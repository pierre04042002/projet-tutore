public class Enigme {

    private String enigme; 
    private boolean etat;
    
    public Enigme(String enigme, boolean etat){
        this.enigme = enigme;
        this.etat = etat;
    }

    public String getEnigme(){
        return this.enigme;
    }

    public boolean getEtat(){
        return this.etat;
    }
}
