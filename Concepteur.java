import java .sql .*;
import java.util.List;

public class Concepteur{
    private ConnexionMySQL laConnexion;

    public Concepteur(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;
    }

    public boolean rechercherScenarioParNom(String nomScenario)throws SQLException{
        boolean res;
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("select * from SCENARIO where titresc = " + nomScenario);
        if (rs.next()){
            res = true;
        }
        else{
            res= false;
            throw new SQLException("Le joueur " + pseudo + " n'existe pas.");
        }
        return res;
    }

    public boolean rechercherCarteParNom(String nomCarte)throws SQLException{
        boolean res;
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("select * from CARTE where nomca = " + nomCarte);
        if (rs.next()){
            res = true;
        else{
            res = false;
            throw new SQLException("La carte " + nomCarte + " n'existe pas.");
        }
        return res;
    }

    public boolean rechercherEnigmeParNom(String nomEnigme)throws SQLException{
        boolean res;
        Statement st = this.laConnexion.createStatement();
        ResultSet rs = st.executeQuery("select * from ENIGME where nomen = " + nomEnigme);
        if (rs.next()){
            res = true;
        }
        else{
            res = false;
            throw new SQLException("L'enigme " + nomCarte + " n'existe pas.");
        }
        return res;
    }


    public int insererScenario( Scenario scn) throws SQLException{
        PreparedStatement pst = this.laConnexion.prepareStatement("insert into SCENARIO values(?,?,?,?,?,?,?)");
        pst.setInt(1, maxNumScenario()+1);
        pst.setString(2, scn.getTitre());
        pst.setString(3, scn.getResume());

        Blob b=laConnexion.createBlob ();
        b.setBlob(4, scn.getIcone());  // l'image pas compris comment faire

        pst.setDate(5, scn.getTpsMax());
        pst.setDate(6, scn.getDateMiseEnLigne());
        pst.setBoolean(7, (scn.getString(5).charAt(0)=='O'));
        pst.setInt(8, scn.getNumConcepteur());
        pst.executeUpdate();

        PreparedStatement pst2 = this.laConnexion.prepareStatement("insert into TEXTE values(?,?,?,?,?,?,?)");

        return maxNumScenario();
    }

    public void effacerScenario(int num) throws SQLException {
        PreparedStatement pst = this.laConnexion.prepareStatement("delete from SCENARIO where idsc = ?");
        pst.setInt(1, num);
        pst.executeUpdate();
    }





}