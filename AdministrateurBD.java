import java.sql.*;
import java.util.List;
import java.util.ArrayList;

public class AdministrateurBD{

    private ConnexionMySQL laconnexion;

    private AdministrateurBD(ConnexionMySQL laconnexion){
        this.laconnexion = laconnexion;
    }

    public boolean rechercherJoueurParPseudo(String pseudo)throws SQLException{
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select * from UTILISATEUR where pseudoUt = " + pseudo);
        if (rs.next()){
            return true;
        }
        else{
            return false;
        }
    }

    public List<Stat> getStat(String nomJoueur) throws SQLException{
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select pseudoUt, avatarUt, activUt, idPa, titreSc, tpsResolution, gagne from UTILISATEUR natural join JOUER natural join PARTIE natural join CONCERNER natural join SCENARIO where pseudoUt =" + nomJoueur + " order by idPa");
        List<Stat> res = new ArrayList<>();
        if (rs.next()){
            while (rs.next()){
            byte[] avatar = rs.getBytes(1);
            boolean actif = rs.getString(2).charAt(0) == 'O';
            int numPartie = rs.getInt(3);
            String scenario = rs.getString(4);
            Date temps = rs.getDate(5);
            boolean fini = rs.getString(6).charAt(0) == 'O';
            res.add(new Stat(avatar,actif,numPartie,scenario,temps,fini));
            rs.next();
            }
        }
        else{
            throw new SQLException("Joueur " + nomJoueur + " n'existe pas.");
        }
        return res;
    }
}