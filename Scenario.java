import java.sql.*;

public class Scenario{
    
    private byte[] icone;
    private String nom;
    private boolean etat;

    public Scenario(byte[] icone, String nom, Boolean etat){
        this.icone = icone;
        this.nom = nom;
        this.etat = etat;
    }

    public byte[] getIcone(){
        return this.icone;
    }

    public String getNom(){
        return this.nom;
    }

    public boolean getEtat(){
        return this.etat;
    }

}